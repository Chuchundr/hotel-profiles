from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
#from search import views as search_views

urlpatterns = [
    path('', include('hotel.urls', namespace='hotel')),
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
#   path('search', search_views.search, name='search'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
