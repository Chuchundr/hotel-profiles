from __future__ import absolute_import, unicode_literals

import os

from celery import Celery


# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'project.settings')


app = Celery('hotel')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()


app.conf.beat_schedule = {
    'Hotel TicketItems periodic update': {
        'task': 'hotel.tasks.get_ticket_items','schedule': 10.0,
    },
    'Hotel Ticket periodic update': {
        'task': 'hotel.tasks.get_tickets','schedule': 10.0,
    },
    'Hotel Agreement periodic update': {
        'task': 'hotel.tasks.get_agreements','schedule': 10.0,
    },
    'Address Country periodic update': {
        'task': 'address.tasks.get_country','schedule': 10.0,
    },
    'Address Region periodic update': {
        'task': 'address.tasks.get_region','schedule': 10.0,
    },
    'Address City periodic update': {
        'task': 'address.tasks.get_city','schedule': 10.0,
    },
    'Address Area periodic update': {
        'task': 'address.tasks.get_area','schedule': 10.0,
    },
    'Address Street periodic update': {
        'task': 'address.tasks.get_street','schedule': 10.0,
    },
    'Address Address periodic update': {
        'task': 'address.tasks.get_address','schedule': 10.0,
    },
}
