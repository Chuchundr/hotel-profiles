'''
Package: hotel
Description: views
'''
import datetime
from uuid import uuid4
from django.db import transaction
from django.shortcuts import redirect
from django.contrib.auth.decorators import permission_required
from django.utils.decorators import method_decorator
from django.views import generic
from django.views.generic import edit
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy

from address import models as address_models

from hotel import models, forms, tasks, managers
# @method_decorator(permission_required("polls.can_vote"), name="dispatch")


@method_decorator(permission_required('hotel.view_hotel', 'hotel.add_hotel'), name='dispatch')
class HotelListView(generic.ListView):
    '''
    отображение списка отелей
    '''
    model = models.Hotel
    template_name = 'hotel/hotel_list.html'


@method_decorator(permission_required('hotel.view_hotel'), name='dispatch')
class HotelDetailView(edit.ModelFormMixin, generic.DetailView):
    '''
    отобажение деталей отеля
    '''
    model = models.Hotel
    template_name = 'hotel/hotel_detail.html'
    form_class = forms.HotelForm

    def get_context_data(self, *args, **kwargs): #pylint: disable=arguments-differ
        context = super(HotelDetailView, self).get_context_data(*args, **kwargs)
        context['contragent_list'] = models.Contragent.objects.all()
        today = datetime.date.today()
        month = today + datetime.timedelta(days=30)
        diff = (month-today).days+1
        date_list = []
        for day in range(diff):
            date_list.append(today+datetime.timedelta(day))
        context['date_list'] = date_list
        context['basket_form'] = forms.BasketForm
        search_form = forms.SearchForm(self.request.GET)
        context['search_form'] = search_form
        if search_form.is_valid():
            context['date_start'] = search_form.cleaned_data['date_start']
            context['date_end'] = search_form.cleaned_data['date_end']
            manager = managers.ReservationSearchManager()
            search_form.cleaned_data['hotel'] = str(self.object.uuid)
            manager.search(search_form.cleaned_data)
            context['manager'] = manager
        context['basket'] = models.Basket.objects.filter(hotel=str(self.object.uuid))
        return context



@method_decorator(permission_required('hotel.add_hotel'), name='dispatch')
class HotelCreateView(generic.CreateView):
    '''
    отображение создания отелей
    '''
    model = models.Hotel
    form_class = forms.HotelForm
    template_name = 'hotel/hotel_form.html'

    def get_success_url(self):
        return reverse('hotel:address-create', kwargs={'slug': self.object.slug})



@method_decorator(permission_required('address.add_address'), name='dispatch')
class AddressCreateView(generic.CreateView):
    '''
    отображение создания отелей
    '''
    form_class = forms.AddressForm
    model = address_models.Address
    template_name = 'hotel/address_form.html'

    def get_success_url(self):
        return reverse(
            'hotel:contact-create',
            kwargs={'slug': self.kwargs.get('slug')}
        )

    def get_context_data(self, *args, **kwargs): #pylint: disable=arguments-differ
        context = super(AddressCreateView, self).get_context_data(*args, **kwargs)
        context['hotel_slug'] = self.kwargs.get('slug')
        return context

    def form_valid(self, form):
        address = form.save()
        hotel = models.Hotel.objects.get(slug=self.kwargs.get('slug'))
        hotel.address = address
        hotel.save()
        return redirect(self.get_success_url())


@method_decorator(permission_required('hotel.change_contact'), name='dispatch')
class HotelContactUpdateView(generic.UpdateView):
    '''
    отображение обновления и создания отелей
    '''
    model = models.Hotel
    form_class = forms.HotelContactFormSet
    template_name = 'hotel/contact_form.html'

    def get_success_url(self):
        return reverse(
            'hotel:contact-create',
            kwargs={'slug': self.kwargs.get('slug')}
        )


@method_decorator(permission_required('hotel.delete_hotel'), name='dispatch')
class HotelDeleteView(generic.DeleteView):
    '''
    отображение удаления отелей
    '''
    model = models.Hotel
    success_url = reverse_lazy('hotel:hotel-list')


@method_decorator(permission_required('hotel.change_hotel'), name='dispatch')
class HotelUpdateView(edit.UpdateView):
    '''
    отображение обновления отелей
    '''
    model = models.Hotel
    form_class = forms.HotelForm
    template_name = 'hotel/hotel_form.html'

    def get_success_url(self):
        return reverse('hotel:hotel-detail', kwargs={'slug': self.object.slug})


@method_decorator(permission_required('hotel.change_address'), name='dispatch')
class AddressUpdateView(generic.UpdateView):
    '''
    отображение обновления адреса
    '''
    model = address_models.Address
    form_class = forms.AddressForm
    template_name = 'hotel/address_form.html'

    def get_success_url(self):
        return reverse('hotel:hotel-detail',
                       kwargs={
                           'slug': models.Hotel.objects.get(address_id=self.kwargs.get('pk')).slug
                       })


@method_decorator(permission_required('hotel.add_room'), name='dispatch')
class HotelRoomCreateView(generic.CreateView):
    '''
    отображение создания номеров
    '''
    model = models.Hotel
    form_class = forms.RoomCountForm
    template_name = 'hotel/room_form.html'

    def get_context_data(self, *args, **kwargs):
        context = super(HotelRoomCreateView, self).get_context_data(*args, **kwargs)
        context['hotel'] = models.Hotel.objects.get(slug=self.kwargs.get('slug'))
        return context

    def get_success_url(self):
        return reverse('hotel:hotel-detail', kwargs={
            'slug': self.kwargs.get('slug')
        })

    def form_valid(self, form):
        item = form.cleaned_data
        item['parent'] = models.Hotel.objects.get(slug=self.kwargs.get('slug'))
        iters = range(item['count'])
        del item['count']
        del item['uuid']
        rooms = [models.Room(room_type=item['room_type'],
                             bed_type=item['bed_type'],
                             parent=item['parent']) for _ in iters]
        items = models.Room.objects.bulk_create(rooms)
        for item in items:
            tasks.create_or_update_room.delay(item.to_dict())
        return redirect(self.get_success_url())


@method_decorator(permission_required('hotel.change_room'), name='dispatch')
class HotelRoomUpdateView(generic.UpdateView):
    model = models.Hotel
    form_class = forms.RoomFormSet
    template_name = 'hotel/room_update_form.html'
    
    def get_success_url(self):
        return reverse('hotel:hotel-detail', kwargs={'slug': self.kwargs.get('slug')})


@method_decorator(permission_required('hotel.change_hotel'), name='dispatch')
class SubscribtionUpdateView(generic.TemplateView):
    '''
    отображение обновления подписок
    '''
    form_class = forms.SubscribtionForm

    def post(self, request, *args, **kwargs):
        '''
        метод, запускающий таск по обновлению статуса подписки при валидности формы
        '''
        form = self.form_class(request.POST)
        if form.is_valid():
            tasks.update_subscribe.delay(form.cleaned_data)
        else:
            print(form.errors)
        return HttpResponseRedirect(self.get_success_url(form.cleaned_data['contragent_to']))

    def get_success_url(self, *args): #pylint: disable=no-self-use
        '''
        success url для subsribtion update view
        '''
        return reverse_lazy('hotel:hotel-detail', kwargs={
            'slug': models.Hotel.objects.get(uuid=args[0]).slug
        })


@method_decorator(permission_required('hotel.change_agreement'), name='dispatch')
class AgreementUpdateView(generic.edit.UpdateView):
    '''
    отображнеие для неподтвержденных заявок
    TODO пересмотреть механизм
    '''
    model = models.Agreement
    form_class = forms.AgreementForm
    template_name = 'hotel/agreement_form.html'

    def get_context_data(self, *args, **kwargs): #pylint: disable=arguments-differ
        context = super(AgreementUpdateView, self).get_context_data(*args, **kwargs)
        context['ticket_form'] = forms.TicketForm
        return context

    def form_valid(self, form):
        ticket_form = forms.TicketForm(self.request.POST)
        if ticket_form.is_valid():
            ticket_form.save()
        super(AgreementUpdateView, self).form_valid(form)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('hotel:hotel-detail', kwargs={'slug': models.Hotel.objects.get(uuid=self.object.hotel_id).slug})


@method_decorator(permission_required('hotel.view_agreement'), name='dispatch')
class AgreementView(generic.DetailView):
    '''
    отображение для сделок
    '''
    queryset = models.Agreement.objects.all()
    template_name = 'hotel/agreement_form.html'


@method_decorator(permission_required('hotel.add_basket'), name='dispatch')
class BasketCreateView(generic.CreateView):
    '''
    вьюха для создания баскета
    '''
    model = models.Basket
    form_class = forms.BasketForm

    def form_valid(self, form):
        
        if form.is_valid():
            form.cleaned_data['hotel'] = str(form.cleaned_data['hotel'].uuid)
            manager = managers.ReservationSearchManager()
            iters = range(form.cleaned_data['count'])
            date_start = form.cleaned_data['date_start']
            date_end = form.cleaned_data['date_end']
            del form.cleaned_data['count']
            manager.search(form.cleaned_data)
            rooms = [item.to_dict() for item in models.Room.objects.filter(
                room_type=manager.params['room_type'],
                parent_id=manager.params['hotel'],
                bed_type=manager.params['bed_type'])]
            result = [item['uuid'] for item in rooms if item not in manager]
            for i in iters:
                models.Basket.objects.create(hotel=models.Hotel.objects.get(uuid=manager.params['hotel']),
                                             date_start=date_start,
                                             date_end=date_end,
                                             room_type=manager.params['room_type'],
                                             bed_type=manager.params['bed_type'],
                                             room_id=result[i]
                )
        else:
            print(form.errors)
        return HttpResponseRedirect(self.get_success_url())
        
    def get_success_url(self, *args, **kwargs):
        return reverse('hotel:hotel-detail', kwargs={
            'slug': self.kwargs.get('slug')})


@method_decorator(permission_required('hotel.delete_basket'), name='dispatch')
class BasketDeleteView(generic.DeleteView):
    '''
    вьюха для удаления из баскета
    '''
    model = models.Basket
    form_class = forms.BasketForm

    def get_success_url(self, *args, **kwargs):
        return reverse('hotel:hotel-detail', kwargs={
            'slug': models.Hotel.objects.get(uuid=self.object.hotel.uuid).slug
        })

    
@method_decorator(permission_required('hotel.add_agreement'), name='dispatch')
class AgreementCreateView(generic.CreateView):
    """
    View для создания Agreement, Ticket, TicketItems
    """
    model = models.Agreement
    form_class = forms.AgreementForm
    template_name = 'hotel/agreement_create_form.html'
    
    def get_success_url(self):
        return reverse('hotel:hotel-detail', kwargs={'slug': self.kwargs.get('slug')})
    
    def form_valid(self, form):
        agreement = form.save(commit=False)
        agreement.slug = str(uuid4())[0:8]
        agreement.save()
        ticket = models.Basket.objects.filter(hotel=agreement.hotel)
        ticket = ticket[0]
        with transaction.atomic():
            row = models.Ticket(
                parent=agreement,
                contragent_from=ticket.hotel.uuid,
                contragent_to=ticket.hotel.uuid,
                status='1'
            )
            row.save()
        items = models.Basket.objects.filter(hotel=agreement.hotel.uuid)
        for item in items:
            with transaction.atomic():
                row = models.TicketItem(
                    parent=agreement,
                    room=item.room,
                    date_start=item.date_start,
                    date_end=item.date_end
                )
                row.save()
                items.delete()
        return redirect(self.get_success_url())
