'''
package: hotel
description: tasks
'''
from __future__ import absolute_import, unicode_literals
import logging

from kombu.exceptions import OperationalError
from django.db import IntegrityError
from aiohttp.client_exceptions import ClientConnectorError, ClientResponseError

from django.conf import settings
from django.db import OperationalError as django_operational_error

from hotel import models, managers

from project.celery import app


LOGGER = logging.getLogger(__name__)

#hotels
@app.task(bind=True)
def update_or_create_hotel(self, data):
    '''
    таск для обновления или создания отеля в API
    '''
    try:
        manager = managers.HotelManager()
        return manager.update_or_create(data)
    except IntegrityError as message:
        raise IntegrityError(message)
    except OperationalError as msg:
        raise self.retry(exc=msg)
    except (ClientResponseError, ClientConnectorError) as msg:
        raise self.retry(exc=msg)
    except django_operational_error as msg:
        raise self.retry(exc=msg)
    except ValueError as msg:
        raise self.retry(exc=msg)
        

@app.task(bind=True)
def delete_hotel(self, data):
    '''
    таск для удаления отеля из API
    '''
    try:
        manager = managers.HotelManager()
        return manager.delete_item(data)
    except IntegrityError as message:
        raise IntegrityError(message)
    except OperationalError as msg:
        raise self.retry(exc=msg)
    except django_operational_error as msg:
        raise self.retry(exc=msg)
    except (ClientResponseError, ClientConnectorError) as msg:
        raise self.retry(exc=msg)

#contacts
@app.task(bind=True)
def create_or_update_contact(self, data):
    '''
    таск для создания или обновления контактов в API
    '''
    try:
        manager = managers.HotelContactManager()
        return manager.update_or_create(data)
    except IntegrityError as message:
        raise IntegrityError(message)
    except OperationalError as msg:
        raise self.retry(exc=msg)
    except django_operational_error as msg:
        raise self.retry(exc=msg)
    except (ClientResponseError, ClientConnectorError) as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def delete_contact(self, data):
    '''
    таск для удаления контакта из API
    '''
    try:
        manager = managers.HotelContactManager()
        return manager.delete_item(data)
    except IntegrityError as message:
        raise IntegrityError(message)
    except OperationalError as msg:
        raise self.retry(exc=msg)
    except django_operational_error as msg:
        raise self.retry(exc=msg)
    except (ClientResponseError, ClientConnectorError) as msg:
        raise self.retry(exc=msg)


#rooms
@app.task(bind=True)
def create_or_update_room(self, data):
    '''
    таск для создания номерного фонда в API
    '''
    try:
        manager = managers.RoomManager()
        return manager.update_or_create(data)
    except IntegrityError as message:
        raise IntegrityError(message)
    except OperationalError as msg:
        raise self.retry(exc=msg)
    except django_operational_error as msg:
        raise self.retry(exc=msg)
    except (ClientResponseError, ClientConnectorError) as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def delete_room(self, data):
    '''
    таск для удаления номерного фонда из API
    '''
    try:
        manager = managers.RoomManager()
        return manager.delete_item(data)
    except IntegrityError as message:
        raise IntegrityError(message)
    except OperationalError as msg:
        raise self.retry(exc=msg)
    except django_operational_error as msg:
        raise self.retry(exc=msg)
    except (ClientResponseError, ClientConnectorError) as msg:
        raise self.retry(exc=msg)
    
    
#subscribtions
@app.task(bind=True)
def get_subscribtions(self):
    '''
    таск, подтягивающий все заявки на подписку
    '''
    try:
        manager = managers.SubscribtionManager()
        result = []

        for hotel in models.Hotel.objects.all():
            for item in manager.get_list():
                if item.get('is_approved') is None and item.get('contragent_to') == hotel.to_dict().get('uuid'):
                    result.append(item)
        return result
    except IntegrityError as message:
        raise IntegrityError(message)
    except OperationalError as msg:
        raise self.retry(exc=msg)
    except django_operational_error as msg:
        raise self.retry(exc=msg)
    except (ClientResponseError, ClientConnectorError) as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def update_subscribe(self, data):
    '''
    таск, обновляет статус подписки и записывает данные по агентству в таблицу контрагенты
    '''
    try:
        subscribtion_manager = managers.SubscribtionManager()
        agency_manager = managers.AgencyManager()
        for hotel in models.Hotel.objects.all():
            for item in subscribtion_manager.get_list():
                if item.get('contragent_to') == hotel.to_dict().get('uuid') and data.get('is_approved') is True:
                    agency = agency_manager.get_item_by_uuid(item.get('contragent_from'))
                    agency['address_id'] = agency.pop('address')
                    models.Contragent.objects.update_or_create(**agency)
        return subscribtion_manager.update_or_create(data)
    except IntegrityError as message:
        raise IntegrityError(message)
    except django_operational_error as msg:
        raise self.retry(exc=msg)
    except (ClientResponseError, ClientConnectorError) as msg:
        raise self.retry(exc=msg)


# Действия с Tickets
@app.task(bind=True)
def get_agreements(self):
    '''
    periodic task, подтягивает тикеты
    '''
    try:
        manager = managers.AgreementManager()
        for hotel in models.Hotel.objects.all():
            for item in manager.get_list():
                if item.get('hotel') == str(hotel.uuid) and item.get('process_status') == 0:
                    del item['created']
                    del item['edited']
                    models.Agreement.objects.update_or_create(
                        hotel_id=item.pop('hotel'),
                        agent_id=item.pop('agent'),
                        **item)
    except OperationalError as msg:
        raise self.retry(exc=msg)
    except django_operational_error as msg:
        raise self.retry(exc=msg)
    except (ClientResponseError, ClientConnectorError) as msg:
        raise self.retry(exc=msg)
    except IntegrityError as msg:
        raise self.retry(exc=msg)

@app.task(bind=True)
def update_or_create_agreement(self, data):
    '''
    обновляет тикет, в data передаются данные из метода to_dict модели Ticket
    '''
    try:
        manager = managers.AgreementManager()
        manager.update_or_create(data)
    except IntegrityError as message:
        raise IntegrityError(message)
    except django_operational_error as msg:
        raise self.retry(exc=msg)
    except (ClientResponseError, ClientConnectorError) as msg:
        raise self.retry(exc=msg)

@app.task(bind=True)
def get_tickets(self):
    '''
    подтягивает тикеты
    '''
    if not settings.CORE_API:
        try:
            ticket_manager = managers.TicketManager()
            for agreement in models.Agreement.objects.all():
                for item in ticket_manager.get_list():
                    if item.get('parent') == str(agreement.uuid):
                        del item['created']
                        del item['edited']
                        models.Ticket.objects.update_or_create(parent_id=item.pop('parent'), **item)
        except IntegrityError as message:
            raise IntegrityError(message)
        except django_operational_error as msg:
            raise self.retry(exc=msg)
        except (ClientResponseError, ClientConnectorError) as msg:
            raise self.retry(exc=msg)

@app.task(bind=True)
def get_ticket_items(self):
    '''
    подтягивает items тикета
    '''
    if not settings.CORE_API:
        try:
            item_manager = managers.TicketItemManager()
            for agreement in models.Agreement.objects.all():
                for item in item_manager.get_list():
                    if item.get('parent') == str(agreement.uuid):
                        models.TicketItem.objects.update_or_create(room_id=item.pop('room'),
                                                                   parent_id=item.pop('parent'), **item)
        except IntegrityError as message:
            raise IntegrityError(message)
        except django_operational_error as msg:
            raise self.retry(exc=msg)
        except (ClientResponseError, ClientConnectorError) as msg:
            raise self.retry(exc=msg)
    

@app.task(bind=True)
def update_or_create_ticket(self, data):
    '''
    обновляет тикет, в data передаются данные из метода to_dict модели Ticket
    '''
    try:
        manager = managers.TicketManager()
        manager.update_or_create(data)
    except IntegrityError as message:
        raise IntegrityError(message)
    except django_operational_error as msg:
        raise self.retry(exc=msg)
    except (ClientResponseError, ClientConnectorError) as msg:
        raise self.retry(exc=msg)

@app.task(bind=True)
def update_or_create_ticket_item(self, data):
    '''
    создает ticket_items
    '''
    try:
        manager = managers.TicketItemManager()
        manager.update_or_create(data)
    except IntegrityError as message:
        raise IntegrityError(message)
    except django_operational_error as msg:
        raise self.retry(exc=msg)
    except (ClientResponseError, ClientConnectorError) as msg:
        raise self.retry(exc=msg)
