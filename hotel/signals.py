'''
description: signals
'''
from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver
from django.conf import settings

from hotel import models, tasks

#hotels
@receiver(post_save, sender=models.Hotel)
def update_or_create_hotel_hook(sender, instance, using, **kwargs):
    '''
    сигнал, срабатывающий при создании или обновлении отеля
    '''
    if not settings.CORE_API:
        tasks.update_or_create_hotel.delay(instance.to_dict())

@receiver(post_delete, sender=models.Hotel)
def delete_hotel_hook(sender, instance, using, **kwargs):
    '''
    сигнал, срабатывающий при удалении отеля
    '''
    if not settings.CORE_API:
        tasks.delete_hotel.delay(instance.to_dict())

#contacts
@receiver(post_save, sender=models.Contact)
def create_or_update_contact_hook(sender, instance, using, **kwargs):
    '''
    сигнал, срабатывающий при создании или обновлении контакта
    '''
    if not settings.CORE_API:
        tasks.create_or_update_contact.delay(instance.to_dict())

@receiver(post_delete, sender=models.Contact)
def delete_contact_hook(sender, instance, using, **kwargs):
    '''
    сигнал, срабатывающий при удалении контакта
    '''
    if not settings.CORE_API:
        tasks.delete_contact.delay(instance.to_dict())

#rooms
@receiver(post_save, sender=models.Room)
def create_or_update_room_hook(sender, instance, using, **kwargs):
    '''
    сигнал, срабатывающий при создании или обновлении номеров
    '''
    if not settings.CORE_API:
        tasks.create_or_update_room.delay(instance.to_dict())


@receiver(post_delete, sender=models.Room)
def delete_room_hook(sender, instance, using, **kwargs):
    '''
    сигнал, срабатывающий при удалении номеров
    '''
    if not settings.CORE_API:
        tasks.delete_room.delay(instance.to_dict())


#tickets
@receiver(post_save, sender=models.Agreement)
def update_or_create_agreement_hook(sender, instance, using, **kwargs):
    '''
    сигнал, срабатывающий при создании или обновлении сделки
    '''
    if not settings.CORE_API:
        tasks.update_or_create_agreement.delay(instance.to_dict())


@receiver(post_save, sender=models.Ticket)
def update_or_create_ticket_hook(sender, instance, using, **kwargs):
    if not settings.CORE_API:
        tasks.update_or_create_ticket.delay(instance.to_dict())


@receiver(post_save, sender=models.TicketItem)
def update_or_create_ticket_item_hook(sender, instance, using, **kwargs):
    '''
    сигнал, срабатывающий при создании ticket item
    '''
    if not settings.CORE_API:
        tasks.update_or_create_ticket_item.delay(instance.to_dict())
