'''
package: hotel
description: admin panel
'''
from django.contrib import admin
from sorl.thumbnail.admin import AdminImageMixin

from hotel import models


class HotelContactInline(admin.TabularInline):
    '''
    форма для контактов на странице отеля
    '''
    model = models.Contact

@admin.register(models.Hotel)
class HotelAdmin(AdminImageMixin, admin.ModelAdmin):
    '''
    страница для отелей
    '''
    inlines = [HotelContactInline]
    list_display = ['name', ]

@admin.register(models.Room)
class RoomAdmin(admin.ModelAdmin):
    '''
    страница для номеров
    '''
    list_display = ['parent', 'room_type', 'bed_type', 'desc']
    list_filter = ['parent__name', 'room_type', 'bed_type']

@admin.register(models.Contragent)
class ContragentAdmin(admin.ModelAdmin):
    '''
    страница для контаргентов
    '''


class TicketItemInline(admin.TabularInline):
    '''
    форма для содержимого тикетов
    '''
    model = models.TicketItem

class TicketInline(admin.TabularInline):
    '''
    форма для тикетов
    '''
    model = models.Ticket

@admin.register(models.Agreement)
class AgreementtAdmin(admin.ModelAdmin):
    '''
    форма для сделок
    '''
    inlines = [TicketItemInline, TicketInline, ]
