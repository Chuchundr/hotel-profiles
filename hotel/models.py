'''
Package: hotel
description: hotel.models
'''
from uuid import uuid4
from django.db import models
from django.urls import reverse
from pytils.translit import slugify  # для перевода кирилицы

from sorl.thumbnail import ImageField

from address.models import Address

class Hotel(models.Model):
    '''
    Модель для отелей
    '''
    uuid = models.UUIDField(primary_key=True, default=uuid4)
    name = models.CharField(max_length=250, verbose_name='Название')
    address = models.ForeignKey(Address, null=True, on_delete=models.SET_NULL, verbose_name='Адрес отеля')
    photo = ImageField(blank=True, upload_to='hotel-logo', verbose_name='Логотип')
    slug = models.SlugField(max_length=250, editable=False, db_index=True, blank=True, verbose_name='ЧПУ')
    site = models.URLField(verbose_name='Сайт')
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата добавления')
    edited = models.DateTimeField(auto_now=True, editable=False, verbose_name='Дата редактирования')

    def get_absolute_url(self):
        '''
        Возвращает абсолютный путь для HotelDetailView
        '''
        return reverse('profiles:hotel-detail', kwargs={'slug': self.slug})

    def __str__(self):
        return f'{self.name}'

    def to_dict(self):
        '''
        Предподгатавливает дикт для передачи в АПИ
        '''
        if self.address:
            address = str(self.address.uuid)
        else:
            address = None
        return  {
            'uuid': str(self.uuid),
            'name': self.name,
            'address': address,
            'site': self.site,
        }

    def save(self, *args, **kwargs): # pylint: disable=arguments-differ
        self.slug = slugify(f'{self.name} {self.uuid}')
        super(Hotel, self).save(*args, **kwargs)

    class Meta:
        unique_together = ['name', 'address']
        verbose_name = 'Отель'
        verbose_name_plural = 'Отели'


class Contact(models.Model):
    '''
    Модель для контактов отеля
    '''
    uuid = models.UUIDField(primary_key=True, default=uuid4)
    parent = models.ForeignKey(Hotel, on_delete=models.CASCADE, related_name='contacts', verbose_name='Название отеля')
    name = models.CharField(max_length=100, verbose_name='Cлужбa')
    phone = models.CharField(max_length=15, blank=True, verbose_name='Tелефон')
    email = models.EmailField(blank=True, verbose_name='email')
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата добавления')
    edited = models.DateTimeField(auto_now=True, editable=False, verbose_name='Дата редактирования')

    def to_dict(self):
        '''
        Предподгатавливает дикт для передачи в АПИ
        '''
        return {
            'uuid': str(self.uuid),
            'parent': str(self.parent.uuid),
            'name': self.name,
            'phone': self.phone,
            'email': self.email,
        }

    def __str__(self):
        return f'{self.name}, {self.phone}, {self.email}'
    
    class Meta:
        unique_together = ['parent', 'name']
        verbose_name = 'Контакты службы'
        verbose_name_plural = 'Контакты служб'


class Room(models.Model):
    '''
    Модель для номеров
    '''
    BED_TYPE = (
        ('Single', 'Single'),
        ('Double', 'Double'),
        ('Triple', 'Triple')
    )
    ROOM_TYPE = (
        ('Lux', 'Lux'),
        ('Lux/2', 'Lux/2'),
        ('Standartx2', 'Standartx2'),
        ('Standart', 'Standart')
    )
    uuid = models.UUIDField(primary_key=True, default=uuid4)
    parent = models.ForeignKey(Hotel, on_delete=models.CASCADE, related_name='rooms', verbose_name='Отель')
    desc = models.CharField(max_length=100, blank=True, verbose_name='Краткое описание')
    bed_type = models.CharField(max_length=6, choices=BED_TYPE, null=False, blank=False, verbose_name='Тип кровати')
    room_type = models.CharField(max_length=10, choices=ROOM_TYPE, null=False, blank=False, verbose_name='Тип номера')
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата добавления')
    edited = models.DateTimeField(auto_now=True, editable=False, verbose_name='Дата редактирования')

    def to_dict(self):
        '''
        Предподгатавливает дикт для передачи в АПИ
        '''
        return {
            'uuid': str(self.uuid),
            'parent': str(self.parent.uuid),
            'desc': self.desc,
            'bed_type': self.bed_type,
            'room_type': self.room_type,
        }

    def __str__(self):
        return f'{self.parent.name}: {self.room_type}, {self.bed_type}'

    class Meta:
        verbose_name = 'Номер'
        verbose_name_plural = 'Номера'

class Contragent(models.Model):
    '''
    Модель для контрагентов
    '''
    uuid = models.UUIDField(primary_key=True, default=uuid4)
    name = models.CharField(max_length=250, default='NO NAME', verbose_name='Название контрагента')
    address = models.ForeignKey(
        Address,
        on_delete=models.SET_NULL,
        related_name='agencies',
        null=True,
        blank=True,
        verbose_name='Адрес контрагента'
    )
    slug = models.SlugField(editable=False, db_index=True, blank=True, verbose_name='ЧПУ')
    site = models.URLField(blank=True, verbose_name='Адрес сайта')
    photo = models.ImageField(upload_to='contr', blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата добавления')
    edited = models.DateTimeField(auto_now=True, editable=False, verbose_name='Дата редактирования')

    def __str__(self):
        return self.name

    def to_dict(self):
        '''
        Предподгатавливает дикт для передачи в АПИ
        '''
        return {
            'uuid': self.uuid,
            'name': self.name,
            'address': self.address.uuid,
            'site': self.site
        }

    class Meta:
        verbose_name = 'Контрагента'
        verbose_name_plural = 'Контрагенты'
        unique_together = ['name', 'address']

class Agreement(models.Model):
    '''
    Модель для сделок
    '''
    STATUS = (
        (0, 'None'),
        (1, 'Approved'),
        (2, 'Canceled')
    )
    uuid = models.UUIDField(primary_key=True, default=uuid4, )
    slug = models.SlugField(max_length=8, db_index=True, verbose_name='Ref Key')
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE, related_name='agreement_hotel')
    agent = models.ForeignKey(Contragent, on_delete=models.CASCADE, null=True, related_name='agreement_contragent')
    process_status = models.PositiveSmallIntegerField(choices=STATUS, default=0, verbose_name='Статус брони')
    created = models.DateTimeField(auto_now_add=True, editable=False)
    edited = models.DateTimeField(auto_now=True, editable=False)

    def __str__(self):
        return self.slug

    def to_dict(self):
        '''
        Предподгатавливает дикт для передачи в АПИ
        '''
        if self.agent is not None:
            return {
                'uuid': str(self.uuid),
                'slug': self.slug,
                'hotel': str(self.hotel.uuid),
                'agent': str(self.agent.uuid),
                'process_status': self.process_status,
            }
        return  {
            'uuid': str(self.uuid),
            'slug': self.slug,
            'hotel': str(self.hotel.uuid),
            'process_status': self.process_status,
        }
        

    class Meta:
        verbose_name = 'Бронь'
        verbose_name_plural = 'Бронирование'


class Ticket(models.Model):
    '''
    Модель для тикетов
    '''
    STATUS = (
        (0, 'None'),
        (1, 'Approved'),
        (2, 'Canceled')
    )
    uuid = models.UUIDField(primary_key=True, default=uuid4)
    parent = models.ForeignKey(Agreement,
                               db_index=True,
                               on_delete=models.CASCADE,
                               related_name='tickets')
    contragent_from = models.UUIDField(verbose_name='От кого')
    contragent_to = models.UUIDField(verbose_name='Кому')
    status = models.PositiveSmallIntegerField(choices=STATUS, default=0, verbose_name='Process status')
    created = models.DateTimeField(auto_now_add=True, editable=False)
    edited = models.DateTimeField(auto_now=True, editable=False)

    def to_dict(self):
        '''
        Предподгатавливает дикт для передачи в АПИ
        '''
        return {
            'uuid': str(self.uuid),
            'contragent_from': str(self.contragent_from),
            'contragent_to': str(self.contragent_to),
            'status': self.status,
            'parent': str(self.parent.uuid)
        }

    class Meta:
        verbose_name = 'Заявка'
        verbose_name_plural = 'Заявки'
        ordering = ['-created']


class TicketItem(models.Model):
    '''
    Модель для содержимого тикета
    '''
    uuid = models.UUIDField(primary_key=True, default=uuid4)
    parent = models.ForeignKey(Agreement,
                               on_delete=models.CASCADE,
                               db_index=True,
                               related_name='items',
                               verbose_name='Отель')
    room = models.ForeignKey(Room, on_delete=models.CASCADE, related_name='item_room', verbose_name='номер')
    date_start = models.DateTimeField(verbose_name='дата заезда')
    date_end = models.DateTimeField(verbose_name='дата выезда')

    def to_dict(self):
        '''
        Предподгатавливает дикт для передачи в АПИ
        '''
        return {
            'uuid': str(self.uuid),
            'parent': str(self.parent.uuid),
            'room': str(self.room.uuid),
            'date_start': str(self.date_start),
            'date_end': str(self.date_end),
        }

    def __str__(self):
        return str(self.uuid)

    class Meta:
        verbose_name = 'Объект'
        verbose_name_plural = 'Объекты'


class Basket(models.Model):
    """
    Таблица для Basket
    """
    ROOM_TYPE = Room.ROOM_TYPE
    BED_TYPE = Room.BED_TYPE
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE, null=True, related_name='basket_hotel')
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата добавления')
    edited = models.DateTimeField(auto_now=True, editable=False, verbose_name='Дата редактирования')
    room_type = models.CharField(choices=ROOM_TYPE, max_length=250, verbose_name='тип номера')
    bed_type = models.CharField(choices=BED_TYPE, max_length=250, verbose_name='тип кровати')
    room = models.ForeignKey(Room, on_delete=models.CASCADE, null=True, verbose_name='Номер')
    date_start = models.DateTimeField(blank=True, null=True, verbose_name='Дата с')
    date_end = models.DateTimeField(blank=True, null=True, verbose_name='Дата по')
    

    class Meta:
        verbose_name = 'в корзину'
        verbose_name_plural = 'в корзине'
        
    def to_dict(self):
        return {
            'hotel': str(self.hotel.uuid),
            'date_start': self.date_start,
            'date_end': self.date_end,
            'room_type': self.room_type,
            'bed_type': self.bed_type,
            'room': str(self.room.uuid)
        }
        
        
    def get_absolute_url(self):
        '''
        Перенапрявляет на страницу детализации basket
        '''
        return reverse('agency:basket-detail', kwargs={'pk': self.pk})
