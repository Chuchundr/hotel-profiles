'''package: hotel, description: urls'''
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from hotel import views

app_name = 'profiles'

urlpatterns = [
    path('', views.HotelListView.as_view(), name='hotel-list'),
    path('hotel/create', views.HotelCreateView.as_view(), name='hotel-create'),
    path('hotel/update/<slug:slug>', views.HotelUpdateView.as_view(), name='hotel-update'),
    path('hotel/detail/<slug:slug>', views.HotelDetailView.as_view(), name='hotel-detail'),
    path('hotel/<slug:slug>/create-address/', views.AddressCreateView.as_view(), name='address-create'),
    path('hotel/<slug:slug>/create_contact', views.HotelContactUpdateView.as_view(), name='contact-create'),
    path('hotel/<uuid:pk>/update-address', views.AddressUpdateView.as_view(), name='address-update'),
    path('hotel/delete/<slug:slug>', views.HotelDeleteView.as_view(), name='hotel-delete'),
    path('hotel/<slug:slug>/create-room', views.HotelRoomCreateView.as_view(), name='room-create'),
    path('hotel/<slug:slug>/update-room', views.HotelRoomUpdateView.as_view(), name='room-update'),
    path('subscribtions/accept/<uuid:pk>', views.SubscribtionUpdateView.as_view(), name='subscribtion-accept'),
    path('agreement/create/<slug:slug>', views.AgreementCreateView.as_view(), name='agreement-create'),
    path('agreement/update/<slug:slug>', views.AgreementUpdateView.as_view(), name='agreement-update'),
    path('agreement/<slug:slug>', views.AgreementView.as_view(), name='agreement'),
    path('basket/create/<slug:slug>', views.BasketCreateView.as_view(), name='basket-create')
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
