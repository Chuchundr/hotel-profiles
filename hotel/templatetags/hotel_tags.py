'''
Package: hotel
Description: templatetags.hotel_tags
'''
import datetime
from django import template
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from aiohttp.client_exceptions import ClientConnectorError

from address import models as address_models

from hotel import models, managers


register = template.Library()

#AGENCY
@register.simple_tag
def agency_by_uuid(value):
    '''
    для поиска агентства по UUID из локальной базы
    '''
    try:
        return models.Contragent.objects.get(uuid=value)
    except ValidationError:
        return ''
    except ObjectDoesNotExist:
        return ''


@register.simple_tag
def agency_from_api_by_uuid(value):
    '''
    для поиска агентства по UUID из API
    '''
    try:
        manager = managers.AgencyManager()
        return manager.get_item_by_uuid(value)
    except ClientConnectorError:
        return str(None)
    except IndexError:
        return str(None)

#HOTEL
@register.simple_tag
def get_by_uuid(value):
    '''
    для поиска отеля или контрагента по UUID
    '''
    try:
        return str(models.Hotel.objects.get(uuid=value).name)
    except ObjectDoesNotExist:
        return str(models.Contragent.objects.get(uuid=value).name)


#ADDRESS
@register.simple_tag
def address_by_uuid(value):
    '''
    для поиска адреса по UUID
    '''
    try:
        return str(address_models.Address.objects.get(uuid=value))
    except address_models.Address.DoesNotExist:
        return str(None)
    except ValidationError:
        return str(None)


#SUBSCRIBTIONS
@register.simple_tag
def subscribe_count(value):
    '''
    для подсчета общего количества подтвержденных подписок
    '''
    try:
        manager = managers.SubscribtionManager()
        return len([item for item in manager.get_list()
                    if item.get('contragent_to') == value and item.get('is_approved') is True])
    except ClientConnectorError:
        return str(None)
    except IndexError:
        return str(None)

@register.simple_tag
def subscribe_false(value):
    '''
    для поиска заявок со статусом False
    '''
    try:
        manager = managers.SubscribtionManager()
        return [item for item in manager.get_list()
                if item.get('contragent_to') == str(value) and item.get('is_approved') is False]
    except ClientConnectorError:
        return str(None)
    except IndexError:
        return str(None)

@register.simple_tag
def subscribe_true(value):
    '''
    для поиска заявок со статусом True
    '''
    try:
        manager = managers.SubscribtionManager()
        return [item for item in manager.get_list()
                if item.get('contragent_to') == str(value) and item.get('is_approved') is True]
    except ClientConnectorError:
        return str(None)
    except IndexError:
        return str(None)
    
@register.simple_tag
def subscribe_none(value):
    '''
    для поиска заявок со статусом None
    '''
    try:
        manager = managers.SubscribtionManager()
        return [item for item in manager.get_list()
                if item.get('contragent_to') == str(value) and item.get('is_approved') is None]
    except ClientConnectorError:
        return str(None)
    except IndexError:
        return str(None)


@register.simple_tag
def room_types(kwargs):
    """
    шаблонный тег для группировки номеров по типу кровати и номера
    """
    types = []
    for item in kwargs:
        types_dict = {
            'count': models.Room.objects.filter(
                parent=item.parent,
                bed_type=item.bed_type,
                room_type=item.room_type).count(),
            'room_type': item.room_type,
            'bed_type': item.bed_type}
        if types_dict not in types:
            types.append(types_dict)
    return types

@register.simple_tag
def reservation_search(value):
    '''
    тэг возвращает даныые по броням
    '''
    try:
        manager = managers.ReservationSearchManager()
        manager.params = {'hotel': str(value)}
        manager.get_list()
        reservation_list = manager.items
        types = []
        for item in reservation_list:
            item['date'] = datetime.datetime.fromisoformat(item['date']).date()
            for item in reservation_list:
                types_dict = {'count': int(),
                              'room_type': item['room_type'],
                              'bed_type': item['bed_type'],
                              'dates': []}
                counter = list(set([r['room'] for r in reservation_list \
                                    if item['room_type'] == r['room_type'] \
                                    and item['bed_type'] == r['bed_type']
                ]))
                types_dict['count'] = len(counter)
                types_dict['dates'] = set([r['date'] for r in reservation_list \
                                           if item['room'] == r['room'] and item['date'] == r['date']])
                if types_dict not in types:
                    types.append(types_dict)
        return types
    except TypeError:
        return None

@register.simple_tag
def room_search(fn):

    try:
        room_total = models.Hotel.objects.get(uuid=str(fn.params['hotel'])).rooms.filter(room_type=fn.params['room_type'],
                                                                                         bed_type=fn.params['bed_type']).count()
        search_result = fn._room_counter()
        result = {'room_type': fn.params['room_type'],
                  'bed_type': fn.params['bed_type'],
                  'count':  reversed(range(1, (room_total-search_result['count']+1)))}
        return result
    except AttributeError:
        return ''

'''
@register.simple_tag
def basket_grouper(data):
    
'''
