'''
package: hotel
description: managers
'''
from libraries.base import BaseManager, BaseSearchManager # pylint: disable=no-name-in-module
from libraries.managers import (
    BaseFilterHandler,
    DaterangeFilterHandler,
    StrFilterHandler,
    MultichoiceFilterHandler
        )
from hotel import models

class HotelManager(BaseManager): # pylint: disable=too-few-public-methods
    '''
    менеджер для отелей
    '''
    URI = '/api/hotel/hotel/'

class HotelContactManager(BaseManager): # pylint: disable=too-few-public-methods
    '''
    менеджер для контактов отеля
    '''
    URI = '/api/hotel/contact/'

class RoomManager(BaseManager): # pylint: disable=too-few-public-methods
    '''
    менеджер для номеров
    '''
    URI = '/api/hotel/room/'

class RoomPropertyManager(BaseManager): # pylint: disable=too-few-public-methods
    '''
    менеджер для особенностей номеров
    '''
    URI = '/api/hotel/room_property/'

class ReservationManager(BaseManager): # pylint: disable=too-few-public-methods
    '''
    менеджер для броней
    '''
    URI = '/api/hotel/reservation/'

class AgencyManager(BaseManager): # pylint: disable=too-few-public-methods
    '''
    менеджер для агентств
    '''
    URI = '/api/agency/agent/'

class SubscribtionManager(BaseManager): # pylint: disable=too-few-public-methods
    '''
    менеджер для подписок
    '''
    URI = '/api/SubScribe/'

class TicketManager(BaseManager): # pylint: disable=too-few-public-methods
    '''
    менеджер для тикетов
    '''
    URI = '/api/coreapi/ticket/'

class TicketItemManager(BaseManager): # pylint: disable=too-few-public-methods
    '''
    менеджер для содержимого тикетов
    '''
    URI = '/api/coreapi/ticketitem/'

class AgreementManager(BaseManager):
    '''
    менеджер для поиска
    '''
    URI = '/api/coreapi/agreement/'

class AgreementSearchManager(BaseManager):
    '''
    менеджер для поиска
    '''
    URI = '/api/search/agreement/'

'''
def reserved_rooms(fn):
    def wrapper(self, data):
        fn(self, data)
        result_rooms = set([item['room'] for item in self.get_list()])
        hotel = models.Hotel.objects.get(uuid=self.params.get('hotel'))
        for room in hotel.rooms.all():
            if room.uuid not in result_rooms and room.room_type == self.params['room_type'] and room.bed_type == self.params['bed_type']:
                rooms = {
                    'room_type': room.room_type,
                    'bed_type': room.bed_type,
                    'count': hotel.rooms.filter(room_type=self.params['room_type'],
                                                      bed_type=self.params['bed_type']).count() - len(result_rooms)}
        return rooms
    return wrapper
'''

class ReservationSearchManager(BaseSearchManager):
    """
    Менеджер для Поиска номеров
    """
    rooms = {}
    URI = '/api/search/reservation/'
    filters = BaseFilterHandler()
    filters.set_next(DaterangeFilterHandler()).set_next(StrFilterHandler())


    def _room_grouper(self):
        for item in self.items:
            self.rooms['room_type'] = item['room_type']
            self.rooms['bed_type'] = item['bed_type']
        return self.rooms

    def _room_counter(self):
        try:
            room_number = len(list(set([item['room'] for item in self.items])))
        except TypeError:
            room_number = 0
        self.rooms['count'] = room_number
        return self.rooms

    def __repr__(self):
        return repr(self.items)

    def __contains__(self, item):
        try:
            result = [item['room'] for item in self.items]
        except TypeError:
            result = []
        return item['uuid'] in result

class RoomSearchManager(BaseSearchManager):
    URI = '/api/search/reservation/'
    filters = BaseFilterHandler()
    filters.set_next(StrFilterHandler())
