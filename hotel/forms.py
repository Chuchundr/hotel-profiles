'''
package: hotel
description: forms
'''
from django import forms

from address import models as address_models
from hotel import models


class HotelForm(forms.ModelForm):
    '''
    форма для отелей
    '''

    class Meta:
        model = models.Hotel
        fields = ['name', 'site', 'photo']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Введите название отеля'}),
            'site': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Введите адрес сайта'}),
        }


class AddressForm(forms.ModelForm):
    '''
    форма для адреса
    '''
    class Meta:
        model = address_models.Address
        fields = '__all__'
        widgets = {
            'uuid': forms.HiddenInput(),
            'country': forms.Select(attrs={'class': 'form-control'}),
            'region': forms.Select(attrs={'class': 'form-control'}),
            'city': forms.Select(attrs={'class': 'form-control'}),
            'area': forms.Select(attrs={'class': 'form-control'}),
            'street': forms.Select(attrs={'class': 'form-control'}),
            'house': forms.TextInput(attrs={'class': 'form-control'}),
            'flat': forms.TextInput(attrs={'class': 'form-control'}),
            'index': forms.TextInput(attrs={'class': 'form-control'}),
        }


class RoomCountForm(forms.ModelForm):
    '''
    форма для номеров
    '''
    count = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control'}))

    class Meta:
        model = models.Room
        fields = '__all__'
        widgets = {
            'uuid': forms.HiddenInput(),
            'parent': forms.HiddenInput(),
            'desc': forms.TextInput(attrs={'class': 'form-control'}),
            'bed_type': forms.Select(attrs={'class': 'form-control'}),
            'room_type': forms.Select(attrs={'class': 'form-control'}),
        }


class RoomForm(forms.ModelForm):
    '''
    форма для номеров
    '''
    model = models.Room
    fields = ['uuid', 'parent', 'room_type', 'bed_type', 'desc']
    widgets = {
        'uuid': forms.HiddenInput(),
        'parent': forms.HiddenInput(),
        'room_type': forms.Select(attrs={'class': 'form-control'}),
        'bed_type': forms.Select(attrs={'class': 'form-control'}),
        'desc': forms.TextInput(attrs={'class': 'form-control'})
    }

class HotelContactForm(forms.ModelForm):
    '''
    форма для контактов
    '''
    class Meta:
        model = models.Contact
        fields = '__all__'
        widgets = {
            'uuid': forms.HiddenInput(),
            'name':forms.TextInput(attrs={'class': 'form-control'}),
            'phone':forms.TextInput(attrs={'class': 'form-control'}),
            'email':forms.TextInput(attrs={'class': 'form-control'})
        }


class SubscribtionForm(forms.Form):
    '''
    форма для подписок
    '''
    uuid = forms.CharField(widget=forms.HiddenInput())
    contragent_from = forms.CharField(widget=forms.HiddenInput())
    contragent_to = forms.CharField(widget=forms.HiddenInput())
    is_approved = forms.NullBooleanField(required=False)


class AgreementForm(forms.ModelForm):
    '''
    форма для создания agreement
    '''
    class Meta:
        model = models.Agreement
        fields = ['hotel', 'process_status']
        widgets = {
            'hotel': forms.HiddenInput(),
            'process_status': forms.HiddenInput()
        }



class TicketForm(forms.ModelForm):
    '''
    форма для тикетов
    '''
    class Meta:
        model = models.Ticket
        fields = ['uuid', 'parent', 'contragent_from', 'contragent_to', 'status']
        widgets = {
            'parent': forms.HiddenInput(),
            'uuid': forms.HiddenInput(),
            'contragent_from': forms.HiddenInput(),
            'contragent_to': forms.HiddenInput()
        }


class TicketItemForm(forms.ModelForm):
    '''
    форма для содержимого тикетов
    '''
    parent = forms.UUIDField(widget=forms.HiddenInput(), required=False)

    class Meta:
        model = models.TicketItem
        fields = ['uuid', 'room', 'date_start', 'date_end', 'parent']
        widgets = {
            'room': forms.HiddenInput(),
            'date_start': forms.HiddenInput(),
            'date_end': forms.HiddenInput(),
        }

class SearchForm(forms.Form):
    '''
    форма для поиска
    '''
    ROOM_CHOICES = (
        ('Lux', 'Lux'),
        ('Lux/2', 'Lux/2'),
        ('Standart', 'Standart'),
        ('Standartx2', 'Standartx2')
    )
    BED_CHOICES = (
        ('Single', 'Single'),
        ('Double', 'Double'),
        ('Triple', 'Triple')
    )
    date_start = forms.DateField(widget=forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}))
    date_end = forms.DateField(widget=forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}))
    room_type = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), choices=ROOM_CHOICES)
    bed_type = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), choices=BED_CHOICES)
    hotel = forms.CharField(widget=forms.HiddenInput, required=False)

class BasketForm(forms.ModelForm):
    '''
    форма для баскета
    '''

    count = forms.IntegerField(widget=forms.Select(attrs={'class': 'form-control'}))
    date_start = forms.DateField()
    date_end = forms.DateField()
    
    class Meta:
        fields = ['hotel', 'room_type', 'bed_type']
        model = models.Basket
        widgets = {
            'hotel': forms.HiddenInput(),
            'room_type': forms.HiddenInput(),
            'bed_type': forms.HiddenInput()
        }
            
'''
class TicketItemCountForm(forms.ModelForm):
   
    count = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control'}))
    room_type = forms.CharField(widget=forms.Select(attrs={'class': 'form-control'}))
    bed_type = forms.CharField(widget=forms.Select(attrs={'class': 'form-control'}))
    
    class Meta:
        model = models.TicketItem
        fields = ['room', 'date_start', 'date_end', 'parent']
        widgets = {
            'room': forms.HiddenInput(),
            'date_start': forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}),
            'date_end': forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}),
            'parent': forms.HiddenInput()
        }
'''

HotelContactFormSet = forms.inlineformset_factory(
    models.Hotel,
    models.Contact,
    form=HotelContactForm,
    extra=1,
    can_delete=False)


RoomFormSet = forms.inlineformset_factory(
    models.Hotel,
    models.Room,
    fields=['room_type', 'bed_type', 'desc'],
    form=RoomForm,
    extra=0,
    widgets={
        'room_type': forms.Select(attrs={'class': 'form-control'}),
        'bed_type': forms.Select(attrs={'class': 'form-control'}),
        'desc': forms.TextInput(attrs={'class': 'form-control'})
    }
)

TicketItemFormSet = forms.formset_factory(form=TicketItemForm, can_delete=False)
