'''
package: hotel
description: tests
'''
from uuid import uuid4
from django.test import TestCase
from django.test import Client
from django.urls import reverse
from address import models as address_models
from hotel import forms
from hotel import models


class HotelTestCase(TestCase):
    '''
    тесты для создания отелей, адресов и контактов
    '''
    fixtures = ['fixture_auth.json']

    def setUp(self):
        self.client = Client()
        self.client.login(username='admin', password='123654atm')

    def test_hotel_createview_statuscode(self):
        '''
        проверка статуса при переходе по урлу
        '''
        response = self.client.get(reverse('hotel:hotel-create'))
        self.assertEqual(response.status_code, 200)

    def test_hotel_createview_context(self):
        '''
        проверка наличия в контексте формы
        '''
        response = self.client.get(reverse('hotel:hotel-create'))
        self.assertIsInstance(response.context['form'], forms.HotelForm)

    def test_hotel_createview_form(self):
        '''
        проверка отработки пост запроса и перехода
        по success_url
        '''
        response = self.client.post(reverse('hotel:hotel-create'), {'name': 'hotel', 'site': 'hotel.uz'})
        hotel = models.Hotel.objects.last()
        self.assertEqual(hotel.name, 'hotel')
        self.assertRedirects(response, reverse('hotel:address-create', kwargs={'slug': hotel.slug}))


class AddressTestCase(TestCase):
    '''
    тесты по созданию адресов
    '''
    fixtures = ['fixture_auth.json']
    def setUp(self):
        self.client = Client()
        self.client.login(username='admin', password='123654atm')
        self.hotel = models.Hotel.objects.create(name='hotel', site='hotel.uz')
        self.country = address_models.Country.objects.create(country='Узбекистан')
        self.region = address_models.Region.objects.create(region='Ташкентская обл.', parent=self.country)
        self.city = address_models.City.objects.create(city='Ташкент', parent=self.region)
        self.area = address_models.Area.objects.create(area='Мирабад', parent=self.city)
        self.street = address_models.Street.objects.create(street='Ш. Руставели', parent=self.area)

    def test_address_create_view_statuscode(self):
        '''
        проверка статуса при переходе по урлу
        '''
        slug = self.hotel.slug
        response = self.client.get(reverse('hotel:address-create', kwargs={'slug': slug}))
        self.assertEqual(response.status_code, 200)

    def test_address_create_view_context(self):
        '''
        проверка наличия формы в контексте
        '''
        slug = self.hotel.slug
        response = self.client.get(reverse('hotel:address-create', kwargs={'slug': slug}))
        self.assertIsInstance(response.context['form'], forms.AddressForm)

    def test_address_create_view_form(self):
        '''
        проверка отработки пост запроса
        '''
        slug = self.hotel.slug
        response = self.client.post(reverse('hotel:address-create', kwargs={'slug': slug}),
                                    {'uuid': uuid4(),
                                     'country': self.country.uuid,
                                     'region': self.region.uuid,
                                     'city': self.city.uuid,
                                     'area': self.area.uuid,
                                     'street': self.street.uuid,
                                     'house': '1',
                                     'flat': '1'
                                    })
        self.assertRedirects(response, reverse('hotel:contact-create', kwargs={'slug': slug}))

class ContactTestCase(TestCase):
    '''
    тесты для контактов
    '''
    fixtures = ['fixture_auth.json']

    def setUp(self):
        self.client = Client()
        self.client.login(username='admin', password='123654atm')
        self.hotel = models.Hotel.objects.create(name='hotel', site='hotel.uz')

    def test_contact_create_view_statuscode(self):
        '''
        проверка статуса при переходе по урлу
        '''
        response = self.client.get(reverse('hotel:contact-create', kwargs={'slug': self.hotel.slug}))
        self.assertEqual(response.status_code, 200)

    def test_contact_create_view_context(self):
        '''
        проверка наличия формы в контексте
        '''
        response = self.client.get(reverse('hotel:contact-create', kwargs={'slug': self.hotel.slug}))
        self.assertIsInstance(response.context['form'], forms.HotelContactFormSet)
        
class AgreementTestCase(TestCase):
    '''
    тесты для сделок
    '''
    fixtures = ['fixture_auth.json']

    def setUp(self):
        self.client = Client()
        self.client.login(username='admin', password='123654atm')
        self.hotel = models.Hotel.objects.create(name='hotel', site='hotel.uz')
        self.contragent = models.Contragent.objects.create(name='agent', site='agent.uz')
        self.agreement = models.Agreement.objects.create(slug='fc1cdcd0',
                                                         hotel=self.hotel,
                                                         agent=self.contragent,
                                                         process_status='0')

    def test_agreement_update_view_status_code(self):
        '''
        проверка статуса при переходе по урлу
        '''
        response = self.client.get(reverse('hotel:agreement-update', kwargs={'slug': self.agreement.slug}))
        self.assertEqual(response.status_code, 200)

    def test_agreement_update_view_context(self):
        '''
        проверка наличия ticket_form в контексте
        '''
        response = self.client.get(reverse('hotel:agreement-update', kwargs={'slug': self.agreement.slug}))
        form = response.context['ticket_form']()
        self.assertIsInstance(form, forms.TicketForm)

    def test_agreement_update_view_form_valid(self):
        '''
        проверка пост запроса и перехода на
        success_url
        '''
        response = self.client.post(reverse('hotel:agreement-update', kwargs={'slug': self.agreement.slug}),
                                    {'process_status': '1',
                                     'uuid': str(uuid4()),
                                     'parent': self.agreement.uuid,
                                     'contragent_from': str(self.hotel.uuid),
                                     'contragent_to': str(self.contragent.uuid),
                                     'status': '1'
                                    })
        self.assertEqual(models.Ticket.objects.last().status, 1)
        self.assertRedirects(response, reverse('hotel:hotel-detail', kwargs={'slug': self.hotel.slug}))
