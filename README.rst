=============================
Django package: Hotel profiles
=============================

.. image:: https://badge.fury.io/py/gallery.svg
    :target: https://badge.fury.io/py/gallery

.. image:: https://travis-ci.org/dcopm999/gallery.svg?branch=master
    :target: https://travis-ci.org/dcopm999/gallery

.. image:: https://codecov.io/gh/dcopm999/gallery/branch/master/graph/badge.svg
    :target: https://codecov.io/gh/dcopm999/gallery

Hotel profiles

Documentation
-------------

The full documentation is at https://gallery.readthedocs.io.

Quickstart
----------

Install Django hotel profile::

    pip install profile

Add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'profiles',
        ...
    )

Add Django simple photo gallery's URL patterns:

.. code-block:: python


    urlpatterns = [
        ...
        path('profiles/', include('profiles.urls', namespace='profiles')),
        ...
    ]

Features
--------

* TODO

Running Tests
-------------

Does the code actually work?

::

    source <YOURVIRTUALENV>/bin/activate
    (myenv) $ pip install tox
    (myenv) $ tox

Credits
-------

Tools used in rendering this package:

*  Cookiecutter_
*  `cookiecutter-djangopackage`_

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-djangopackage`: https://github.com/pydanny/cookiecutter-djangopackage
